---
id: beneficiary-consult
title: Consulta si eres beneficiario y tu punto de retiro
sidebar_label: Consulta tu punto de retiro
slug: /beneficiary-consult
---

Guía para consultar si eres beneficiario del bono

## Consulta
Para conocer si una persona es beneficiario del bono haga click [aqui](http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/bonus/consult). Se mostrará una pantalla como la siguiente.

![img](../../../static/img/beneficiary/Beneficiario-Consulta-Sistema-Swing.png)
Tendrá que ingresar el código de familia en el campo que se muestra (``Ingresar codigo de hogar``) para conocer si eres beneficiario y conocer tu punto de retiro.

Si el codigo ingresado es el correcto se le mostrará su punto de retiro mostrandose los siguientes datos
- __Nombre__ del punto de retiro
- __Dirección__ del punto de retiro
- __Fecha asignada__ para el retiro del bono
- __Hora asignada__ para el retiro del punto de retiro

![img](../../../static/img/beneficiary/Beneficiario-Consulta-Satisfactoria-Sistema-Swing.png)

:::caution
En el caso el código ingresado es incorrecto o no es beneficiario del cronograma actual se mostrará un error.
:::

![img](../../../static/img/beneficiary/Beneficiario-Consulta-Error-Sistema-Swing.png)