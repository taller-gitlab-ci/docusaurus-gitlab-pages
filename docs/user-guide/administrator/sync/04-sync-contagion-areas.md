---
id: admin-sync-contagion-areas
title: Áreas de contagio sincronizadas
sidebar_label: Áreas de contagio sincronizadas
slug: /sync-contagion-areas
---

La tabla de áreas de contagio muestra los siguientes datos:
- El __código__ del area de contagio
- El código de __ubigeo__
- El __departamento__ al que pertenece el area de contagio
- La __provincia__ al que pertenece el area de contagio
- El __distrito__ al que pertenece el area de contagio
- La __dirección__ del area de contagio
- El __porcentaje de contagios__
- La __fecha de registro__
- La __fecha en la que fue actualizada__

## Visualizar las Áreas de contagio sincronizadas
Para ver la tabla de sincronización primero debe hacer click en el boton de sincronización de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Puntos de retiro para ver la tabla con los puntos de retiro de la última sincronización.

![img](../../../../static/img/admin/sync/Admin-sync-Contagion-areas.png)