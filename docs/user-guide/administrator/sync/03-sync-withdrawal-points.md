---
id: admin-sync-withdrawal-points
title: Puntos de retiro sincronizados
sidebar_label: Puntos de retiro sincronizados
slug: /sync-withdrawal-points
---

Los puntos de retiro sincronizados muestra los siguientes datos:
- El __código__ del punto de retiro
- La __agencia__ del punto de retiro
- El código de __ubigeo__
- El __departamento__ al que pertenece el punto de retiro
- La __provincia__ al que pertenece el punto de retiro
- El __distrito__ al que pertenece el punto de retiro
- La __dirección__ del punto de retiro
- El __horario de atención de lunes a viernes__
- El __horario de atención los sábados__
- La __fecha de registro__
- La __fecha en la que fue actualizada__

## Visualizar los puntos de retiro sincronizados
Para ver la tabla de sincronización primero debe hacer click en el boton de sincronización de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Puntos de retiro para ver la tabla con los puntos de retiro de la última sincronización.

![img](../../../../static/img/admin/sync/Admin-sync-Withdrawal-points.png)
