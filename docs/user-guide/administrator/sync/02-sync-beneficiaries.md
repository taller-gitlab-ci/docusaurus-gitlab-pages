---
id: admin-sync-beneficiary
title: Beneficiarios sincronizados
sidebar_label: Beneficiarios sincronizados
slug: /sync-beneficiaries
---

La tabla de beneficiarios sincronizados muestra los siguientes datos:
- El __código__ del punto de retiro
- El código de __ubigeo__
- El __departamento__ al que pertenece el beneficiario
- La __provincia__ al que pertenece el beneficiario
- El __distrito__ al que pertenece el beneficiario
- La __fecha de registro__
- La __fecha en la que fue actualizada__
- El __estado__ del beneficiario (Activo o inactivo)

## Visualizar los beneficiarios sincronizados
Para ver la tabla de sincronización primero debe hacer click en el boton de sincronización de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Beneficiarios para ver la tabla con los beneficiarios de la última sincronización.

![img](../../../../static/img/admin/sync/Admin-sync-Beneficiarios.png)
