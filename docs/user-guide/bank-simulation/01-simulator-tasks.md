---
id: simulator-tasks
title: Actividades del simulador
sidebar_label: Actividades del simulador
slug: /simulator-tasks
---

## Simulador
Este es un simulador de un punto de retiro o un banco para poder registrar los retiros realizados por los beneficiarios. Para acceder haga click [aqui](http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/sim). Se mostrará un pantalla como la siguiente imagen.

![img](../../../static/img/sim/Simulación-Sistema-Swing.png)

## Seleccionar el punto de retiro
Para seleccionar el punto de retiro desde donde se registraran los retiros del bono debe dirigirse al primer recuadro gris y colocar el código del punto de retiro, la cual es única para cada banco. Se mostrará el nombre del punto de retiro y su dirección.

![img](../../../static/img/sim/Simulación-Banco-Seleccionado-Sistema-Swing.png)

:::caution
En el caso de que se coloque un código erroneo se muestra un mensaje de error.
:::

![img](../../../static/img/sim/Simulación-Banco-Seleccionado-Error-Sistema-Swing.png)

## Registrar el recogo de un bono
Para registrar el recogo de un bono, previamente debe haberse elegido el punto de retiro (Ver la sección anterior)

Si tiene seleccionado un punto de retiro, puede completar en el segundo cuadro gris con el código del beneficiario. En este se muestra el punto de retiro asignado junto con la fecha asignada. En el caso de que el punto de retiro coincide con el punto actual, así como la fecha asignada coincide con el día de la consulta se podrá registrar el cobro del bono.

![img](../../../static/img/sim/Simulación-Beneficiario-Seleccionado-Correcto-Sistema-Swing.png)

:::info
En el caso de que el beneficiario ya haya realizado el cobro se mostrará un mensaje como se muestra en la imagen
:::

![img](../../../static/img/sim/Simulación-Beneficiario-Cobrado-Sistema-Swing.png)

:::caution
En el caso de que no coincide el punto de retiro seleccionado con el punto asignado al beneficiario o el día de la consulta con la fecha asignada se debe registrar una incidencia.
:::

![img](../../../static/img/sim/Simulación-Beneficiario-Seleccionado-Sistema-Swing.png) 

:::caution
En el caso que el código de beneficiaro no exista o ya no forma parte del cronograma actual se mostrará un mensaje como el de la siguiente imagen.
:::

![img](../../../static/img/sim/Simulación-Beneficiario-Error-Sistema-Swing.png)