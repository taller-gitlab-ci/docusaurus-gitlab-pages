---
id: installation-start
title: Guía de instalación
sidebar_label: Cómo iniciar
slug: /
---

El Sistema Swing utiliza un conjunto de servicios de la nube para poder ofrecer las funcionalidades principales.
Además, presenta una base de datos PostgreSQL. Se detallan los pasos a seguir ara poder realizar el despliegue del
sistema en un ambiente de producciòn o desarrollo.

## Infraestructura del Sistema
El sistema cuenta con los siguientes servicios externos:

| Servicio              | Uso                                                                                                                               |
|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| AWS S3                | Usado para guardar los archivos de datos a sincronizar                                                                            |
| AWS Elastic Beanstalk | Usado para desplegar los tres contenedores principales del sistema: `swing-nginx-proxy`, `swing-web-client` y `swing-app`         |
| AWS Batch             | Usado para ejecutar los trabajos de sincronización de datos y generación de cronogramas: `swing-scheduler` y `swing-synchronizer` |
| AWS RDS               | Usado para gestionar la base de datos relacional PostGreSQL                                                                       |

En el siguiente __diagrama de despliegue__, se refleja el uso de los servicios principales como `AWS Elastic Beanstalk` de plomo,
y los demás servicios de color verde.

![img](../../static/img/diagrams/deployment-v2.png)

## Estructura de la guía de instalación
Esta guía se dividirá en dos secciones:
1. configuración de las imágenes docker de los programas
2. configuración de los servicios de AWS y despliegue
