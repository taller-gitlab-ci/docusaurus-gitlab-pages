---
id: installation-services-storage
title: Servicio para Guardado de Archivos
sidebar_label: archivos S3
slug: /installation/services/storage
---

El servicio de AWS S3 es usado para el almacenamiento de los archivos de datos de carga para la sincronización de
beneficiarios, puntos de retiro y zonas de contagio.

Este servicio es usado por dos contenedores:
- `swing-app`, quien carga los archivos de datos dados por el usuario administrador
- `swing-synchronizer`, quien descarga el archivo de datos a sincronizar y lo procesa como un proceso batch

## Variables de entorno
```properties
# Servicio AWS S3
STORAGE_AWS_REGION=
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=
STORAGE_AWS_CREDENTIALS_SECRET_KEY=
STORAGE_AWS_BUCKET_NAME=
```

## Pasos
### Paso 1
Crear el bucket del S3. Guardar el nombre del bucket para ser utilizado por los dos contenedores descritos anteriomente:
```properties
STORAGE_AWS_BUCKET_NAME=swing-app-bucket
```

![img](../../../static/img/services/aws-s3-01-create.png)

### Paso 2
Crear el bucket e identificar las credenciales de llave de acceso y llave secreta para el acceso al servicio de S3:

```properties
# Servicio AWS S3
STORAGE_AWS_REGION=<us-east-1>
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=<access-key>
STORAGE_AWS_CREDENTIALS_SECRET_KEY=<secret-key>
```

![img](../../../static/img/services/aws-s3-02-end.png)
