---
id: installation-services-beanstalk
title: Servicio para despliegue de Apps
sidebar_label: despliegue EBS
slug: /installation/services/beanstalk
---

Por último, luego de haber iniciado los servicios de guardado de archivos con AWS S3, una base de datos disponible en
AWS RDS y los programas batch para sincronización y generación de cronogramas en AWS Batch, se iniciará con el despliegue
de las aplicaciones que conforman el Sistema: `swing-app` y `swing-web-client` con `swing-nginx-proxy`.

Las imágenes a utilizar se pueden encontrar en los siguientes links:

| Imagen              | Link al registro de contenedores del proyecto                                |
|---------------------|------------------------------------------------------------------------------|
| `swing-app`         | https://gitlab.com/fallguys/official/swing-app/container_registry/1592103    |
| `swing-web-client`  | https://gitlab.com/fallguys/official/swing-client/container_registry/1592681 |
| `swing-nginx-proxy` | https://gitlab.com/fallguys/official/swing-client/container_registry/1592681 |


Se utlizarán para este manual, las siguientes imágenes:

| Imagen              | Imagen con tag a utilizar                                                    |
|---------------------|------------------------------------------------------------------------------|
| `swing-app`         | registry.gitlab.com/fallguys/official/swing-app:release-b4ade530             |
| `swing-web-client`  | registry.gitlab.com/fallguys/official/swing-client:web-release-2beeab88      |
| `swing-nginx-proxy` | registry.gitlab.com/fallguys/official/swing-client:proxy-release-2beeab88    |


## Despliegue a Elastic Beanstalk
Se usará AWS EBS para el despliegue de los programas desarrollados. Para ello, se usará
EBS Docker Multicontainer, el cual se maneja con el archivo `Dockerrun.aws.json`.

A continuación, se deben configurar dos archivos `Dockerrun.aws.json`, uno para el backend con `swing-app` y otro
para la interfaz web con `swing-web-client` y `swing-nginx-proxy`.

### Dockerrun con Swing App

**Configuración: [Dockerrun.aws.json](https://gitlab.com/fallguys/official/swing-app/-/blob/master/aws/Dockerrun.aws.json)**

En este caso, se reemplazaría `<version>` con la versión seleccionada: `b4ade530`.

```json
{
  "AWSEBDockerrunVersion": 2,
  "volumes": [
    {
      "name": "logs",
      "host": {
        "sourcePath": "/var/app/current/swing-app-logs"
      }
    }
  ],
  "containerDefinitions": [
    {
      "name": "swing-back-app",
      "image": "registry.gitlab.com/fallguys/official/swing-app:release-<version>",
      "essential": true,
      "memory": 512,
      "portMappings": [
        {
          "hostPort": 80,
          "containerPort": 8080
        }
      ],
      "mountPoints": [
        {
          "sourceVolume": "logs",
          "containerPath": "/app/logs"
        }
      ]
    }
  ]
}
```

### Dockerrun con Swing Web Client y Proxy

**Configuración: [Dockerrun.aws.json](https://gitlab.com/fallguys/official/swing-app/-/blob/master/aws/Dockerrun.aws.json)**

En este caso, se reemplazaría `<version>` con la versión seleccionada: `2beeab88` para ambos (ambos se construyen al mismo tiempo,
por compatibilidad).

```json
{
  "AWSEBDockerrunVersion": 2,
  "containerDefinitions": [
    {
      "name": "swing-web-client",
      "image": "registry.gitlab.com/fallguys/official/swing-client:web-release-<version>",
      "essential": true,
      "memory": 128
    },
    {
      "name": "swing-nginx-proxy",
      "image": "registry.gitlab.com/fallguys/official/swing-client:proxy-release-<version>",
      "essential": true,
      "memory": 128,
      "portMappings": [
        {
          "hostPort": 80,
          "containerPort": 80
        }
      ],
      "links": [
        "swing-web-client"
      ]
    }
  ]
}
```

:::info
El archivo `Dockerrun.aws.json` es una traducción del archivo `docker-compose.yml` presentado en la sección de
Imágenes Docker.
:::

## Despliegue en Beanstalk

:::info
Los pasos para el despliegue son los mismos tanto para `swing-app` como para `swing-web-client` con `swing-nginx-proxy`.
En este caso, se hara referencia al primero.
:::

Luego de configurar el archivo `Dockerrun.aws.json` para `swing-app`, se procederá a generar el ambiente para su
despliegue en AWS Beanstalk.

Para la instalación, se deberá crear una aplicación en EBS y, con esta, un entorno de producción para la aplicación.

### Paso 1
Crear la aplicación en EBS

![img](../../../static/img/services/aws-ebs-01-create-app.png)

Luego, seleccionar la plataforma a utilizar para la aplicación:
- **Plataforma:** Docker
- **Ramificación de la plataforma:** Multi-container Docker running on 64bit Amazon Linux
- **Versión recomendada de la plataforma:** 2.24.0 (este valor puede variar, seleccionar aquella que AWS recomiende)

![img](../../../static/img/services/aws-ebs-02-platform.png)

Después, cargar el archivo `Dockerrun.aws.json` de `swing-app` como código fuente. Este archivo contiene todas las
características de los contenedores a correr y de dónde se obtendrán sus imágenes.

![img](../../../static/img/services/aws-ebs-03-code-v2.png)

Por último, crear la aplicación.

### Paso 2
Luego de crear la aplicación y por defecto el entorno, es necesario configurar este entorno para guardar las variables
de entorno que el contenedor necesita:

Estas variables pueden ser registradas en `Elastic Beanstalk / Entornos / <entorno> / Configuración`, en la zona de Software:

![img](../../../static/img/services/aws-ebs-04-variables.png)

A continuación, debe registrar las siguientes variables de entorno obtenidas de los anteriores servicios configurados:

**Variables de entorno ara `swing-app`:**

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=

# Servicio AWS Batch para Scheduler
SCHEDULER_AWS_REGION=
SCHEDULER_AWS_JOB_NAME=
SCHEDULER_AWS_JOB_QUEUE=
SCHEDULER_AWS_JOB_DEFINITION=
SCHEDULER_AWS_CREDENTIALS_ACCESS_KEY=
SCHEDULER_AWS_CREDENTIALS_SECRET_KEY=

# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_REGION=
SYNCHRONIZER_AWS_JOB_NAME=
SYNCHRONIZER_AWS_JOB_QUEUE=
SYNCHRONIZER_AWS_JOB_DEFINITION=
SYNCHRONIZER_AWS_CREDENTIALS_ACCESS_KEY=
SYNCHRONIZER_AWS_CREDENTIALS_SECRET_KEY=

# Servicio AWS S3
STORAGE_AWS_REGION=
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=
STORAGE_AWS_CREDENTIALS_SECRET_KEY=
STORAGE_AWS_BUCKET_NAME=
```


**Variables de entorno para `swing-web-client` y `swing-nginx-proxy`:**

```properties
NEXT_PUBLIC_API_URL=<url de la API REST de swing-app>
NEXT_PUBLIC_APP_NAME="Sistema Swing"
NEXT_PUBLIC_ALLOW_REGISTER_SCHEDULE_IN_PAST=false
NEXT_PUBLIC_APP_INCIDENT_ID=1
NEXT_PUBLIC_ADMINISTRATOR_NAME=
```

Luego de ello, guardar los cambios y esperar al despliegue de la aplicación.

![img](../../../static/img/services/finish.png)
![img](../../../static/img/services/finish-back.png)
