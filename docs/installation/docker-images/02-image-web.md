---
id: installation-images-web
title: Imagen Swing Web Client y Proxy
sidebar_label: swing web client y proxy
slug: /installation/images/web
---

__Imagen: `swing-web-client` y `swing-nginx-proxy`__

Se describe aquí el archivo `docker-compose.yml` utilizado para el despliegue de las imágenes como contenedores:

```yaml
version: '3.7'

services:
  swing-web-client:
    image: registry.gitlab.com/fallguys/official/swing-web:client-release-<version>
    container_name: swing-web-client
    build:
      context: ../..
      target: release
    env_file:
      - .env

  swing-nginx-proxy:
    image: registry.gitlab.com/fallguys/official/swing-web:proxy-release-<version>
    container_name: swing-nginx-proxy
    build:
      context: ./nginx
      target: release
    environment:
      - WEB_CLIENT_HOST=swing-web-client:3000
    ports:
      - 80:80
    depends_on:
      - swing-web-client
```

## Fuente de la imágenes
La imagen se obtiene del [registro de contenedores de GitLab del proyecto](https://gitlab.com/fallguys/official/swing-client/container_registry/1592681).

![img](../../../static/img/installation/swing-web-client-registry.png)

## Configuración de Web-Client
Esta imagen contiene la aplicación web por donde los usuarios interacturán con el Sistema.
Esta imagen deberá volver a ser construida con las siguientes variables de entorno. La construcción puede realizarse
con el archivo `docker-compose.yml` y el repositorio del proyecto de manera local.

### Variables de entorno
Estas variables de entorno deberán modificarse en el archivo local `.env` del repositorio del proyecto.
```properties
NEXT_PUBLIC_API_URL=<url de la API REST de swing-app>
NEXT_PUBLIC_APP_NAME="Sistema Swing"
NEXT_PUBLIC_ALLOW_REGISTER_SCHEDULE_IN_PAST=false
NEXT_PUBLIC_APP_INCIDENT_ID=1
NEXT_PUBLIC_ADMINISTRATOR_NAME=
```

__Observación:__
- `NEXT_PUBLIC_ALLOW_REGISTER_SCHEDULE_IN_PAST`: usado para indicar si un cronograma puede ser generado con una fecha inicial anterior a la fecha actual.
- `NEXT_PUBLIC_APP_INCIDENT_ID`: no cambiar este valor, hace referencia al tipo de incidencia del cambio de punto de retiro.
- `NEXT_PUBLIC_ADMINISTRATOR_NAME`: nombre del administrador para desplegarlo en el dashboard

## Configuración de Nginx-Proxy
La configuración de la imagen es mínima y ya se encuentra realizada.

__Observación:__ se recalca que esta imagen debe desplegarse como contenedor luego de que el contenedor del Web-Client haya sido levantado.
