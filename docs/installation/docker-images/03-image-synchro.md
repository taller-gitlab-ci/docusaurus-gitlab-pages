---
id: installation-images-synchro
title: Imagen Swing Synchronizer
sidebar_label: swing synchronizer
slug: /installation/images/synchro
---

__Imagen: `swing-synchronizer`__

El programa del Synchronizer es instanciado como un job en AWS Batch, y es ejecutado como respuesta a una petición http realizada
desde Swing-App. Este programa requiere de dos servicios:

- AWS RDS, base de datos: obtendrá los datos de la sincronización a iniciar
- AWS S3, bucket de archivos de datos: con los datos de la sincronización, obtendrá el archivo de datos del S3 para la sincronización

Se describe aquí el archivo `docker-compose.yml` utilizado para el despliegue de esta imagen como contenedor:

```yaml
version: '3.7'

services:
  swing-synchronizer:
    image: registry.gitlab.com/fallguys/official/swing-synchronizer:release-<version>
    container_name: swing-synchronizer
    env_file:
      - production.env
```

## Fuente de la imagen
La imagen se obtiene del [registro de contenedores de GitLab del proyecto](https://gitlab.com/fallguys/official/swing-synchronizer/container_registry/1593712).
Se debe reemplazar la versión `<version>` por la última disponible en el registro de contenedores.

![img](../../../static/img/installation/swing-synchronizer-registry.png)

## Variables de entorno
Las variables de entorno pueden ser manejadas por un archivo `production.env`. Estas variables de entorno hacen referencia
a los dos servicios antes mencionados de AWS:
- AWS RDS para base de datos
- AWS S3 para obtención de archivos de datos a sincronizar

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=

# Servicio AWS S3
STORAGE_AWS_REGION=
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=
STORAGE_AWS_CREDENTIALS_SECRET_KEY=
STORAGE_AWS_BUCKET_NAME=
```
