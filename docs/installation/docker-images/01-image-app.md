---
id: installation-images-app
title: Imagen Swing App
sidebar_label: swing app
slug: /installation/images/app
---

__Imagen: `swing-app`__

Se describe aquí el archivo `docker-compose.yml` utilizado para el despliegue de esta imagen como contenedor:
```yaml
version: '3.7'

services:
  swing-app:
    image: registry.gitlab.com/fallguys/official/swing-app:release-<version>
    container_name: swing-app
    env_file:
      - production.env
    ports:
    - 80:8080
```

## Fuente de la imagen
La imagen se obtiene del [registro de contenedores de GitLab del proyecto](https://gitlab.com/groups/fallguys/official/-/container_registries/1592103).
Se debe reemplazar la versión `<version>` por la última disponible en el registro de contenedores.

![img](../../../static/img/installation/swing-app-registry.png)

## Variables de entorno
Las variables de entorno pueden ser manejadas por un archivo `production.env`. Estas variables de entorno hacen referencia
a cuatro servicios de AWS:
- AWS RDS para base de datos
- AWS Batch para scheduler
- AWS Batch para synchronizer
- AWS S3 para guardado de archivos de datos a sincronizar

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=

# Servicio AWS Batch para Scheduler
SCHEDULER_AWS_REGION=
SCHEDULER_AWS_JOB_NAME=
SCHEDULER_AWS_JOB_QUEUE=
SCHEDULER_AWS_JOB_DEFINITION=
SCHEDULER_AWS_CREDENTIALS_ACCESS_KEY=
SCHEDULER_AWS_CREDENTIALS_SECRET_KEY=

# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_REGION=
SYNCHRONIZER_AWS_JOB_NAME=
SYNCHRONIZER_AWS_JOB_QUEUE=
SYNCHRONIZER_AWS_JOB_DEFINITION=
SYNCHRONIZER_AWS_CREDENTIALS_ACCESS_KEY=
SYNCHRONIZER_AWS_CREDENTIALS_SECRET_KEY=

# Servicio AWS S3
STORAGE_AWS_REGION=
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=
STORAGE_AWS_CREDENTIALS_SECRET_KEY=
STORAGE_AWS_BUCKET_NAME=
```