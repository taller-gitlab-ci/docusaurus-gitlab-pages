---
id: installation-images-scheduler
title: Imagen Swing Scheduler
sidebar_label: swing scheduler
slug: /installation/images/scheduler
---

__Imagen: `swing-scheduler`__

El programa del Scheduler es instanciado como un job en AWS Batch al igual que el Synchronizer, y es ejecutado como
respuesta a una petición http realizada desde Swing-App. Este programa requiere de un único servicio:

- AWS RDS, base de datos: obtendrá los datos del cronograma a iniciar, y de los beneficiarios y puntos de retiro activos.

Se describe aquí el archivo `docker-compose.yml` utilizado para el despliegue de esta imagen como contenedor:

```yaml
version: '3.7'

services:
  swing-scheduler:
    image: registry.gitlab.com/fallguys/official/swing-scheduler:release-<version>
    container_name: swing-scheduler
    env_file:
      - production.env
```

## Fuente de la imagen
La imagen se obtiene del [registro de contenedores de GitLab del proyecto](https://gitlab.com/fallguys/official/swing-scheduler/container_registry/1593675).
Se debe reemplazar la versión `<version>` por la última disponible en el registro de contenedores.

![img](../../../static/img/installation/swing-scheduler-registry.png)

## Variables de entorno
Las variables de entorno pueden ser manejadas por un archivo `production.env`. Estas variables de entorno hacen referencia
al servicio de base de datos de AWS

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=
```
