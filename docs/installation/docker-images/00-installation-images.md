---
id: installation-images
title: Imágenes Docker de programas
sidebar_label: Imágenes Docker
slug: /installation/images
---

El desarrollo del Sistema Swing se ha realizado con imágenes Docker para su empaquetamiento y despliegue. En esta
sección se explicará de dónde se pueden obtener estas imágenes y cómo prepararlas para su despliegue en un ambiente
de producción.

![img](../../../static/img/installation/container-used-white-small.png)

## Imágenes a usar
Las imágenes desarrolladas a usar para todo el funcionamiento del Sistema Swing son las siguientes:

| Imagen                | Despliegue en                         | Contenido                                                                                                                         |
|-----------------------|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| `swing-app`           | Ambiente `app` en AWS EBS             | Contiene lògica de negocio del Sistema como backend, y exhibe una API REST                                                        |
| `swing-web-client`    | Ambiente `web` en AWS EBS             | Contiene las interfaces web del Sistema como frontend por donde el usuario interactua con el Sistema                              |
| `swing-nginx-proxy`   | Ambiente `web` en AWS EBS             | Contiene la configuración de un proxy con Nginx para optimizar las consultas al sistema                                           |
| `swing-scheduler`     | Ambiente `scheduler` en AWS Batch     | Contiene la lógica de generación de cronogramas usando un algoritmo PSO para la distribución de beneficiarios y puntos de retiro  |
| `swing-synchronizer`  | Ambiente `synchronizer` en AWS Batch  | Contiene la lógica de preprocesamiento de datos para su sincronización y carga a la base de datos                                 |

